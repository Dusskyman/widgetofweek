import 'package:flutter/material.dart';

class WOWAnimatedWidget extends StatefulWidget {
  const WOWAnimatedWidget({Key key}) : super(key: key);

  @override
  _WOWAnimatedWidgetState createState() => _WOWAnimatedWidgetState();
}

class _WOWAnimatedWidgetState extends State<WOWAnimatedWidget>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    )..repeat(reverse: true);
  }

  @override
  Widget build(BuildContext context) {
    return WOWTransition(
      width: _controller,
    );
  }
}

class WOWTransition extends AnimatedWidget {
  const WOWTransition({width}) : super(listenable: width);

  Animation<double> get width => listenable;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Container(
        width: 200 * width.value,
        height: 200,
        color: Colors.blueAccent,
      ),
    );
  }
}
