import 'package:flutter/material.dart';

class WOWListTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: ListTile(
        tileColor: Colors.blueAccent[100],
        leading: Icon(Icons.code),
        title: Text('My Tile'),
        subtitle: Text('Yep, it\'s really a tile'),
        trailing: Icon(Icons.accessibility),
      ),
    );
  }
}
