import 'package:flutter/material.dart';

class WOWTweenAnimationBuilder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: TweenAnimationBuilder<Color>(
        tween: ColorTween(
          begin: Colors.amber,
          end: Colors.blue,
        ),
        duration: Duration(seconds: 10),
        builder: (context, value, child) => Container(
          color: value,
        ),
      ),
    );
  }
}
