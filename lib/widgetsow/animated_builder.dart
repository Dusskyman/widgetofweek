import 'package:flutter/material.dart';

class WOWAnimatedBuilder extends StatefulWidget {
  const WOWAnimatedBuilder({Key key}) : super(key: key);

  @override
  _WOWAnimatedBuilderState createState() => _WOWAnimatedBuilderState();
}

class _WOWAnimatedBuilderState extends State<WOWAnimatedBuilder>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animationController,
      builder: (context, child) => InkWell(
        onTap: () {
          if (_animationController.isCompleted) {
            _animationController.reverse();
            return;
          }
          _animationController.forward();
        },
        child: Container(
          height: (400 * _animationController.value) + 100,
          width: 300,
          color: Colors.red,
        ),
      ),
    );
  }
}
