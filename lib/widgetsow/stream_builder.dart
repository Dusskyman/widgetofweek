import 'package:flutter/material.dart';

class WOWStreamBuilder extends StatelessWidget {
  Stream<int> count() async* {
    int i = 1;
    while (i < 10) {
      Future.delayed(Duration(seconds: 1));
      yield i++;
    }
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<int>(
      stream: count(),
      builder: (context, snapshot) {
        return Text(snapshot.data.toString());
      },
    );
  }
}
