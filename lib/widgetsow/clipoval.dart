import 'package:flutter/material.dart';

class WOWClipOval extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: ClipOval(
        child: Container(
          color: Colors.greenAccent,
          width: 100,
          height: 100,
        ),
      ),
    );
  }
}
