import 'package:flutter/material.dart';

class WOWTransform extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Transform(
        transform: Matrix4.skewX(0.5),
        child: Container(
          width: 100,
          height: 100,
          color: Colors.green,
        ),
      ),
    );
  }
}
