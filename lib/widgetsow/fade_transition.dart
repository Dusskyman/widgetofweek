import 'dart:async';

import 'package:flutter/material.dart';

class WOWFadeTransition extends StatefulWidget {
  const WOWFadeTransition({Key key}) : super(key: key);

  @override
  _WOWFadeTransitionState createState() => _WOWFadeTransitionState();
}

class _WOWFadeTransitionState extends State<WOWFadeTransition>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  List<Color> colors = [
    Colors.blue,
    Colors.red,
    Colors.green,
    Colors.yellow,
  ];
  int index = 0;
  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 300),
    );
    controller.addListener(() {
      setState(() {
        if (controller.isCompleted) {
          if (index == colors.length - 1) {
            controller.reverse();
            return index = 0;
          }
          controller.reverse();
          index++;
        }
        if (controller.isDismissed) {
          if (index == colors.length - 1) {
            controller.forward();
            return index = 0;
          }
          controller.forward();
          index++;
        }
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => controller.forward(),
      child: FadeTransition(
        opacity: controller,
        child: Container(
          width: double.infinity,
          height: double.infinity,
          color: colors[index],
        ),
      ),
    );
  }
}
