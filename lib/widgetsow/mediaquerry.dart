import 'package:flutter/material.dart';

class WOWMediaquerry extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var querrySize = MediaQuery.of(context).size;
    return Container(
      width: querrySize.width * 0.5,
      height: querrySize.height * 0.5,
      color: Colors.red,
    );
  }
}
