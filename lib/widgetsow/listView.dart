import 'package:flutter/material.dart';

class WOWListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Container(
          width: 100,
          height: 200,
          color: Colors.green,
        ),
        Container(
          width: 100,
          height: 200,
          color: Colors.red,
        ),
        Container(
          width: 100,
          height: 200,
          color: Colors.green,
        ),
        Container(
          width: 100,
          height: 200,
          color: Colors.red,
        ),
      ],
    );
  }
}
