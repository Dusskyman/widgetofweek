import 'package:flutter/material.dart';

class WOWDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
            width: 50,
            height: 50,
            color: Colors.amber,
          ),
          Container(
            width: 50,
            height: 50,
            color: Colors.blueAccent,
          ),
          Container(
            width: 50,
            height: 50,
            color: Colors.amber,
          ),
          Container(
            width: 50,
            height: 50,
            color: Colors.blueAccent,
          ),
          Container(
            width: 50,
            height: 50,
            color: Colors.amber,
          ),
          Container(
            width: 50,
            height: 50,
            color: Colors.blueAccent,
          ),
          Container(
            width: 50,
            height: 50,
            color: Colors.amber,
          ),
        ],
      ),
    );
  }
}
