import 'package:flutter/material.dart';

class WOWAlertDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Wow this is Alert Dialog!'),
      actions: [
        FlatButton(
          onPressed: () => Navigator.pop(context),
          child: Text('Okay'),
        )
      ],
    );
  }
}
