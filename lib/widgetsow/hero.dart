import 'package:flutter/material.dart';

class WOWHero extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: InkWell(
        onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => WOWHeroSecond(),
          ),
        ),
        child: Center(
          child: Hero(
              tag: 'hero',
              child: SizedBox(width: 100, child: Image.network(url))),
        ),
      ),
    );
  }
}

String url =
    'https://www.pixsy.com/wp-content/uploads/2021/04/ben-sweet-2LowviVHZ-E-unsplash-1.jpeg';

class WOWHeroSecond extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: InkWell(
        onTap: () => Navigator.pop(context),
        child: Center(
          child: Hero(tag: 'hero', child: Image.network(url)),
        ),
      ),
    );
  }
}
