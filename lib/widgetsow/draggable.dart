import 'package:flutter/material.dart';

class WOWDraggble extends StatefulWidget {
  @override
  _WOWDraggbleState createState() => _WOWDraggbleState();
}

class _WOWDraggbleState extends State<WOWDraggble> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Draggable(
            child: Container(
              width: 100,
              height: 100,
              color: Colors.blue,
            ),
            childWhenDragging: Container(
              width: 100,
              height: 100,
              color: Colors.blue,
            ),
            feedback: Container(
              width: 100,
              height: 100,
              color: Colors.grey,
            ),
          ),
        ],
      ),
    );
  }
}
