import 'package:flutter/material.dart';

class WOWAnimatedPadding extends StatefulWidget {
  @override
  _WOWAnimatedPaddingState createState() => _WOWAnimatedPaddingState();
}

class _WOWAnimatedPaddingState extends State<WOWAnimatedPadding> {
  double pad = 0;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          IconButton(
              icon: Icon(Icons.accessibility_new),
              onPressed: () {
                setState(() {
                  if (pad == 0) {
                    return pad = 300;
                  }
                  return pad = 0;
                });
              }),
          AnimatedPadding(
            padding: EdgeInsets.only(bottom: pad),
            duration: Duration(seconds: 1),
          ),
        ],
      ),
    );
  }
}
