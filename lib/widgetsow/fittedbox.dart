import 'package:flutter/material.dart';

class WOWFittedBox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints(
        minHeight: 100,
        minWidth: 100,
        maxWidth: 250,
        maxHeight: 250,
      ),
      child: Container(
        width: 350,
        height: 350,
        color: Colors.blue,
        child: FittedBox(
          child: Text(
            'VERY LONG WORD',
            style: TextStyle(fontSize: 100),
          ),
        ),
      ),
    );
  }
}
