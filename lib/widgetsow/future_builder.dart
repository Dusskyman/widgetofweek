import 'package:flutter/material.dart';

class WOWFutureBuilder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Future.delayed(Duration(seconds: 3), () => 'Hi there'),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Text(
            snapshot.data,
            style: TextStyle(fontSize: 50),
          );
        }
        return CircularProgressIndicator();
      },
    );
  }
}
