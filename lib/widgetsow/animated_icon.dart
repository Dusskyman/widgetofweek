import 'package:flutter/material.dart';

class WOWAnimatedIcon extends StatefulWidget {
  const WOWAnimatedIcon({Key key}) : super(key: key);

  @override
  _WOWAnimatedIconState createState() => _WOWAnimatedIconState();
}

class _WOWAnimatedIconState extends State<WOWAnimatedIcon>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (_animationController.isCompleted) {
          return _animationController.reverse();
        }
        _animationController.forward();
      },
      child: Container(
        alignment: Alignment.center,
        width: 100,
        height: 100,
        color: Colors.grey,
        child: AnimatedIcon(
          icon: AnimatedIcons.list_view,
          progress: _animationController,
        ),
      ),
    );
  }
}
