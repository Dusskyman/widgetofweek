import 'package:flutter/material.dart';

class WOWCheckboxListTile extends StatefulWidget {
  WOWCheckboxListTile({Key key}) : super(key: key);

  @override
  _WOWCheckboxListTileState createState() => _WOWCheckboxListTileState();
}

class _WOWCheckboxListTileState extends State<WOWCheckboxListTile> {
  bool checked = false;

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 300),
      color: checked ? Colors.green : Colors.red,
      child: CheckboxListTile(
        title: Text('Check work of checkbox list tile'),
        value: checked,
        onChanged: (value) {
          setState(() {
            checked = value;
          });
        },
      ),
    );
  }
}
