import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class WOWConstrainedBox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints(
        minHeight: 100,
        minWidth: 100,
        maxWidth: 250,
        maxHeight: 250,
      ),
      child: Container(
        width: 50,
        height: 50,
        color: Colors.blue,
      ),
    );
  }
}
