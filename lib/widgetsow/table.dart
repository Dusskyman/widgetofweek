import 'package:flutter/material.dart';

class WOWTable extends StatelessWidget {
  const WOWTable({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Table(
      defaultColumnWidth: IntrinsicColumnWidth(),
      children: [
        TableRow(
          decoration: BoxDecoration(color: Colors.grey),
          children: [
            Container(
              width: 100,
              height: 20,
              color: Colors.red,
            ),
            SizedBox(width: 20),
            Container(
              width: 20,
              height: 100,
              color: Colors.red,
            ),
            SizedBox(width: 20),
            Container(
              width: 50,
              height: 50,
              color: Colors.red,
            ),
          ],
        ),
        TableRow(
          decoration: BoxDecoration(color: Colors.black12),
          children: [
            Container(
              width: 30,
              height: 70,
              color: Colors.black,
            ),
            SizedBox(width: 20),
            Container(
              width: 30,
              height: 30,
              color: Colors.black,
            ),
            SizedBox(
              width: 20,
            ),
            Container(
              width: 70,
              height: 30,
              color: Colors.black,
            ),
          ],
        )
      ],
    );
  }
}
