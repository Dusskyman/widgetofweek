import 'package:flutter/material.dart';

class WOWAnimatedContainer extends StatefulWidget {
  @override
  _WOWAnimatedContainerState createState() => _WOWAnimatedContainerState();
}

class _WOWAnimatedContainerState extends State<WOWAnimatedContainer> {
  double width = 0;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          IconButton(
              icon: Icon(Icons.accessibility_new),
              onPressed: () {
                setState(() {
                  if (width == 0) {
                    return width = 200;
                  }
                  return width = 0;
                });
              }),
          AnimatedContainer(
            color: Colors.amber,
            duration: Duration(seconds: 1),
            width: width,
            height: 200,
          ),
        ],
      ),
    );
  }
}
