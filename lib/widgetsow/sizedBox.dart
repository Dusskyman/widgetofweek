import 'package:flutter/material.dart';

class WOWSizedBox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(25),
      child: SizedBox.expand(
        child: ColoredBox(color: Colors.indigo),
      ),
    );
  }
}
