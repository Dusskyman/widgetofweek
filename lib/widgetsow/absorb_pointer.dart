import 'package:flutter/material.dart';

class WOWAbsorbPointer extends StatelessWidget {
  const WOWAbsorbPointer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AbsorbPointer(
      child: InkWell(
        onTap: () => print('Tap'),
        child: Container(
          width: 200,
          height: 200,
          color: Colors.green,
        ),
      ),
    );
  }
}
