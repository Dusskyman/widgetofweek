import 'package:flutter/material.dart';
import 'dart:ui' as ui;

class WOWCustomPaint extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var querrySize = MediaQuery.of(context).size;
    return Align(
      alignment: Alignment.topCenter,
      child: CustomPaint(
        painter: CurvedPainter(),
        size: Size(querrySize.width, 137),
      ),
    );
  }
}

class CurvedPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = Colors.teal
      ..strokeWidth = 15
      ..shader = ui.Gradient.linear(
        Offset(size.width * 0.5, 0),
        Offset(size.width * 0.5, size.height),
        [
          Color(0xffffbd00),
          Color(0xffffde81),
        ],
      );

    var path = Path();

    path.moveTo(0, size.height * 0.681);
    path.quadraticBezierTo(size.width * 0.180, size.height, size.width * 0.380,
        size.height * 0.637);
    path.quadraticBezierTo(size.width * 0.495, size.height * 0.295,
        size.width * 0.712, size.height * 0.520);
    path.quadraticBezierTo(
        size.width * 0.988, size.height * 1.111, size.width * 1.11, -30);
    path.lineTo(size.width, 0);
    path.lineTo(0, 0);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
