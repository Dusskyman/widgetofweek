import 'package:flutter/material.dart';

class WOWRichText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: RichText(
        text: TextSpan(children: [
          TextSpan(
            text: 'This my cool text, ',
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.w200),
          ),
          TextSpan(
            text: 'created by Rich text, ',
            style: TextStyle(color: Colors.pink, fontWeight: FontWeight.w400),
          ),
          TextSpan(
            text: 'with different styles',
            style: TextStyle(color: Colors.green, fontWeight: FontWeight.bold),
          ),
        ]),
      ),
    );
  }
}
