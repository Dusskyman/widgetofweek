import 'package:flutter/material.dart';

class WOWDraggableScrollableSheet extends StatefulWidget {
  const WOWDraggableScrollableSheet({Key key}) : super(key: key);

  @override
  _WOWDraggableScrollableSheetState createState() =>
      _WOWDraggableScrollableSheetState();
}

class _WOWDraggableScrollableSheetState
    extends State<WOWDraggableScrollableSheet> {
  @override
  Widget build(BuildContext context) {
    return DraggableScrollableSheet(
      builder: (context, scrollController) => SingleChildScrollView(
        controller: scrollController,
        child: Container(
          height: 1000,
          width: 200,
          color: Colors.amber,
        ),
      ),
    );
  }
}
