import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WOWCupertinoActionSheet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Builder(
        builder: (context) => Center(
          child: IconButton(
            icon: Icon(
              Icons.bubble_chart,
            ),
            onPressed: () => showCupertinoModalPopup(
              context: context,
              builder: (context) => CupertinoActionSheet(
                actions: [
                  CupertinoActionSheetAction(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text('Cool'))
                ],
                title: Text('Wow, this is Cupertino Action Sheet'),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
