import 'package:flutter/material.dart';

class WOWIgnorePointer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        width: 100,
        height: 50,
        child: IgnorePointer(
          ignoring: true,
          child: RaisedButton(
            color: Colors.blue,
            onPressed: () {},
          ),
        ),
      ),
    );
  }
}
