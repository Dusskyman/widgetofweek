import 'package:flutter/material.dart';

class WOWWrap extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.black26,
      child: Wrap(
        spacing: 20,
        runSpacing: 20,
        children: [
          ...List.generate(
              10,
              (index) => Container(
                    color: Colors.deepOrange,
                    width: 100,
                    height: 100,
                  ))
        ],
      ),
    );
  }
}
