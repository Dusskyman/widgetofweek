import 'package:flutter/material.dart';
import 'package:widget_of_week/widgetsow/aspect_ratio.dart';
import 'package:widget_of_week/widgetsow/colorfilter.dart';
import 'package:widget_of_week/widgetsow/hero.dart';

class WOWPageView extends StatelessWidget {
  final controller = PageController();
  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: controller,
      children: [
        Scaffold(body: WOWHero()),
        Scaffold(body: WOWColorFilter()),
        Scaffold(body: WOWAspectRatio()),
      ],
    );
  }
}
