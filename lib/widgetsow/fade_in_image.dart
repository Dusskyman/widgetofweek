import 'package:flutter/material.dart';

class WOWFadeInImage extends StatelessWidget {
  const WOWFadeInImage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FadeInImage.assetNetwork(
      placeholder: 'assets/200.gif',
      image: 'https://hatrabbits.com/wp-content/uploads/2017/01/random.jpg',
    );
  }
}
