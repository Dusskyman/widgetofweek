import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WOWSliders extends StatefulWidget {
  @override
  _WOWSlidersState createState() => _WOWSlidersState();
}

class _WOWSlidersState extends State<WOWSliders> {
  double slider = 0;

  RangeValues rangedSlider = RangeValues(0, 0.5);

  double cupertinoSlider = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Slider(
          value: slider,
          divisions: 3,
          onChanged: (value) {
            setState(() {
              slider = value;
            });
          },
        ),
        CupertinoSlider(
            value: cupertinoSlider,
            onChanged: (value) {
              setState(() {
                cupertinoSlider = value;
              });
            }),
        RangeSlider(
            values: rangedSlider,
            onChanged: (value) {
              setState(() {
                rangedSlider = value;
              });
            })
      ],
    );
  }
}
