import 'package:flutter/material.dart';

class WOWNotificationListener extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return NotificationListener<ScrollEndNotification>(
      onNotification: (notification) {
        print('Scroll Stoped');
        print(notification);
        return true;
      },
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.all(20),
              width: 100,
              height: 100,
              color: Colors.blue,
            ),
            Container(
              margin: EdgeInsets.all(20),
              width: 100,
              height: 100,
              color: Colors.blue,
            ),
            Container(
              margin: EdgeInsets.all(20),
              width: 100,
              height: 100,
              color: Colors.blue,
            ),
            Container(
              margin: EdgeInsets.all(20),
              width: 100,
              height: 100,
              color: Colors.blue,
            ),
            Container(
              margin: EdgeInsets.all(20),
              width: 100,
              height: 100,
              color: Colors.blue,
            ),
            Container(
              margin: EdgeInsets.all(20),
              width: 100,
              height: 100,
              color: Colors.blue,
            ),
          ],
        ),
      ),
    );
  }
}
