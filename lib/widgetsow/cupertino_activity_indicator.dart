import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WOWCupertinoActivityIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: CupertinoActivityIndicator(),
    );
  }
}
