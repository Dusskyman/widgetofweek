import 'package:flutter/material.dart';

class WOWAboutDialog extends StatelessWidget {
  const WOWAboutDialog({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => showAboutDialog(
          context: context,
          applicationName: 'TestName',
          applicationIcon: Icon(Icons.adb),
          applicationVersion: '2.0.1'),
      child: Container(
        width: 200,
        height: 200,
        color: Colors.green,
      ),
    );
  }
}
