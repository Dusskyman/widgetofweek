import 'package:flutter/material.dart';

class WOWPlaceholder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 200,
      height: 200,
      child: Placeholder(
        color: Colors.amber,
        strokeWidth: 3,
        fallbackWidth: 150,
        fallbackHeight: 150,
      ),
    );
  }
}
