import 'package:flutter/material.dart';

class WOWValueListanableBuilder extends StatefulWidget {
  const WOWValueListanableBuilder({Key key}) : super(key: key);

  @override
  _WOWValueListanableBuilderState createState() =>
      _WOWValueListanableBuilderState();
}

class _WOWValueListanableBuilderState extends State<WOWValueListanableBuilder> {
  ValueNotifier<int> valueNotifier;
  @override
  void initState() {
    super.initState();
    valueNotifier = ValueNotifier<int>(0);
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<int>(
      valueListenable: valueNotifier,
      builder: (context, value, child) {
        return InkWell(
          onTap: () {
            valueNotifier.value++;
          },
          child: Container(
            width: 100,
            height: 100,
            color: Colors.amber,
            child: Text(
              '$value',
              style: TextStyle(fontSize: 40),
            ),
          ),
        );
      },
    );
  }
}
