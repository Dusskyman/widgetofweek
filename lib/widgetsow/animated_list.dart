import 'package:flutter/material.dart';

class WOWAnimatedList extends StatelessWidget {
  WOWAnimatedList({Key key}) : super(key: key);
  GlobalKey<AnimatedListState> listkey = GlobalKey<AnimatedListState>();
  List<int> list = List.generate(10, (index) => index);
  Widget slide(context, index, animation) {
    int item = list[index];
    return SlideTransition(
      position: Tween<Offset>(
        begin: Offset(-1, 0),
        end: Offset(0, 0),
      ).animate(animation),
      child: InkWell(
        onTap: () {
          listkey.currentState.removeItem(
              index, (context, animation) => slide(context, index, animation),
              duration: Duration(seconds: 1));
          list.removeAt(index);
        },
        child: Container(
          width: double.infinity,
          height: 100,
          color: Colors.black,
          child: Text(
            '${list[index]}',
            style: TextStyle(
              fontSize: 34,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedList(
      key: listkey,
      initialItemCount: list.length,
      itemBuilder: (context, index, animation) =>
          slide(context, index, animation),
    );
  }
}
