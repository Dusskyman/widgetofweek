import 'package:flutter/material.dart';

class WOWOpacity extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          color: Colors.green,
          height: 100,
          width: 100,
        ),
        Opacity(
          opacity: 0.0,
          child: Container(
            color: Colors.red,
            width: 100,
            height: 100,
          ),
        ),
        Container(
          color: Colors.green,
          width: 100,
          height: 100,
        ),
      ],
    );
  }
}
