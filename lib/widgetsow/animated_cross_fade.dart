import 'package:flutter/material.dart';

class WOWAnimatedCrossFade extends StatefulWidget {
  const WOWAnimatedCrossFade({Key key}) : super(key: key);

  @override
  _WOWAnimatedCrossFadeState createState() => _WOWAnimatedCrossFadeState();
}

class _WOWAnimatedCrossFadeState extends State<WOWAnimatedCrossFade> {
  bool _boo = true;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        setState(() {
          _boo = !_boo;
        });
      },
      child: AnimatedCrossFade(
        crossFadeState:
            _boo ? CrossFadeState.showFirst : CrossFadeState.showSecond,
        duration: Duration(seconds: 1),
        firstChild: Container(
          width: 200,
          height: 200,
          color: Colors.amber,
        ),
        secondChild: Container(
          width: 200,
          height: 200,
          color: Colors.green,
        ),
      ),
    );
  }
}
