import 'package:flutter/material.dart';

class WOWAnimatedSwitcher extends StatefulWidget {
  const WOWAnimatedSwitcher({Key key}) : super(key: key);

  @override
  _WOWAnimatedSwitcherState createState() => _WOWAnimatedSwitcherState();
}

class _WOWAnimatedSwitcherState extends State<WOWAnimatedSwitcher> {
  bool anotherwidget = false;
  Widget wowwidget = Container(width: 100, height: 200, color: Colors.green);

  void setNewWidget() {
    setState(() {
      if (anotherwidget) {
        wowwidget = Container(width: 100, height: 200, color: Colors.green);
      } else {
        wowwidget = Container(width: 200, height: 300, color: Colors.black);
      }
      anotherwidget = !anotherwidget;
    });
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        setNewWidget();
      },
      child: AnimatedSwitcher(duration: Duration(seconds: 1), child: wowwidget),
    );
  }
}
