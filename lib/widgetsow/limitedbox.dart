import 'package:flutter/material.dart';

class WOWLimitedBox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ColoredBox(
      color: Colors.red,
      child: LimitedBox(
        maxWidth: 250,
        maxHeight: 250,
        child: Container(
          width: 500,
          height: 500,
        ),
      ),
    );
  }
}
