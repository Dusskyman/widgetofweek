import 'package:flutter/material.dart';

class WOWTooltip extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Tooltip(
      message: 'Hi there',
      child: ConstrainedBox(
        constraints: BoxConstraints(
          minHeight: 100,
          minWidth: 100,
          maxWidth: 250,
          maxHeight: 250,
        ),
        child: Container(
          width: 350,
          height: 350,
          color: Colors.blue,
          child: FittedBox(
            child: Text(
              'VERY LONG WORD',
              style: TextStyle(fontSize: 100),
            ),
          ),
        ),
      ),
    );
  }
}
