import 'package:flutter/material.dart';

class WOWToggleButtons extends StatefulWidget {
  WOWToggleButtons({Key key}) : super(key: key);

  @override
  _WOWToggleButtonsState createState() => _WOWToggleButtonsState();
}

class _WOWToggleButtonsState extends State<WOWToggleButtons> {
  List<bool> _selection = List.generate(4, (_) => false);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      height: 200,
      child: ToggleButtons(
        isSelected: _selection,
        children: [
          Icon(Icons.cake),
          Icon(Icons.local_activity),
          Icon(Icons.local_airport),
          Icon(Icons.local_atm),
        ],
        onPressed: (index) {
          setState(() {
            _selection[index] = !_selection[index];
          });
        },
      ),
    );
  }
}
