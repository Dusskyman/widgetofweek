import 'package:flutter/material.dart';

class WOWStack extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: SizedBox(
              height: 200,
              width: 200,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Positioned(
                    left: 10,
                    top: 10,
                    child: Container(
                      color: Colors.green,
                      width: 100,
                      height: 100,
                    ),
                  ),
                  Positioned(
                    right: 10,
                    top: 10,
                    child: Container(
                      color: Colors.blue,
                      width: 80,
                      height: 80,
                    ),
                  ),
                  Container(
                    color: Colors.red,
                    width: 60,
                    height: 60,
                  ),
                  Container(
                    color: Colors.yellow,
                    width: 40,
                    height: 40,
                  ),
                ],
              ))),
    );
  }
}
