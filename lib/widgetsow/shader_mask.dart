import 'package:flutter/material.dart';

class WOWShaderMask extends StatelessWidget {
  const WOWShaderMask({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: 200,
      height: 200,
      color: Colors.black,
      child: ShaderMask(
        shaderCallback: (bounds) =>
            LinearGradient(colors: [Colors.yellow[50], Colors.yellow[900]])
                .createShader(bounds),
        child: Text(
          'Burning',
          style: TextStyle(color: Colors.white, fontSize: 40),
        ),
      ),
    );
  }
}
