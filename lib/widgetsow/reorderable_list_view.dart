import 'package:flutter/material.dart';

class WOWReordableListView extends StatefulWidget {
  WOWReordableListView({Key key}) : super(key: key);

  @override
  _WOWReordableListViewState createState() => _WOWReordableListViewState();
}

class _WOWReordableListViewState extends State<WOWReordableListView> {
  List<int> list = List.generate(10, (index) => index);
  @override
  Widget build(BuildContext context) {
    return ReorderableListView(
      onReorder: (oldIndex, newIndex) {
        setState(() {
          if (oldIndex < newIndex) {
            newIndex -= 1;
          }
          final int item = list.removeAt(oldIndex);
          list.insert(newIndex, item);
        });
      },
      children: [
        ...list
            .map(
              (e) => Container(
                key: ValueKey('$e'),
                margin: EdgeInsets.all(10.0),
                width: 100,
                height: 100,
                color: Colors.grey,
                child: Text('$e'),
              ),
            )
            .toList(),
      ],
    );
  }
}
