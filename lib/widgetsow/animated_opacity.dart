import 'package:flutter/material.dart';

class WOWAnimatedOpacity extends StatefulWidget {
  @override
  _WOWAnimatedOpacityState createState() => _WOWAnimatedOpacityState();
}

class _WOWAnimatedOpacityState extends State<WOWAnimatedOpacity> {
  double _opacity = 0;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          IconButton(
              icon: Icon(Icons.accessibility_new),
              onPressed: () {
                setState(() {
                  if (_opacity == 0) {
                    return _opacity = 1;
                  }
                  return _opacity = 0;
                });
              }),
          AnimatedOpacity(
              duration: Duration(seconds: 1),
              opacity: _opacity,
              child: Container(
                width: 100,
                height: 100,
                color: Colors.blue,
              )),
        ],
      ),
    );
  }
}
