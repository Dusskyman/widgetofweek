import 'package:flutter/material.dart';

class WOWSafeArea extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        color: Colors.blueGrey,
        width: double.infinity,
        height: double.infinity,
      ),
    );
  }
}
