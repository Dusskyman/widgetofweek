import 'package:flutter/material.dart';

class WOWFlexible extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Container(
          width: 100,
          height: 100,
          color: Colors.amber,
        ),
        Flexible(
          fit: FlexFit.tight,
          child: Container(
            width: 100,
            color: Colors.blue,
          ),
        ),
        Container(
          width: 100,
          height: 100,
          color: Colors.amber,
        ),
        Flexible(
          fit: FlexFit.loose,
          child: Container(
            width: 100,
            height: 100,
            color: Colors.blue,
          ),
        ),
      ],
    );
  }
}
