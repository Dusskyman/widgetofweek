import 'package:flutter/material.dart';

class WOWListWheelScrollView extends StatefulWidget {
  const WOWListWheelScrollView({Key key}) : super(key: key);

  @override
  _WOWListWheelScrollViewState createState() => _WOWListWheelScrollViewState();
}

class _WOWListWheelScrollViewState extends State<WOWListWheelScrollView> {
  @override
  Widget build(BuildContext context) {
    return ListWheelScrollView(
      itemExtent: 100,
      children: [
        ...List.generate(
            10,
            (index) => Container(
                  width: 200,
                  height: 200,
                  color: Colors.blue,
                ))
      ],
    );
  }
}
