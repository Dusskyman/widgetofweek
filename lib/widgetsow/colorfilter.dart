import 'package:flutter/material.dart';

class WOWColorFilter extends StatelessWidget {
  String url =
      'https://www.pixsy.com/wp-content/uploads/2021/04/ben-sweet-2LowviVHZ-E-unsplash-1.jpeg';
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Filtered',
            style: TextStyle(fontSize: 30),
          ),
          ColorFiltered(
              colorFilter: ColorFilter.mode(Colors.grey, BlendMode.color),
              child: Image.network(url)),
          Text(
            'Raw',
            style: TextStyle(fontSize: 30),
          ),
          Image.network(url),
        ],
      ),
    );
  }
}
