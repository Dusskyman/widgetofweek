import 'package:flutter/material.dart';

class WOWTabBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Tab(
                text: 'First',
              ),
              Tab(
                text: 'Second',
              ),
              Tab(
                text: 'Third',
              ),
            ],
          ),
        ),
        body: WOWTabBarView(),
      ),
    );
  }
}

class WOWTabBarView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return TabBarView(children: [
      Center(
        child: Text('One'),
      ),
      Center(
        child: Text('Two'),
      ),
      Center(
        child: Text('Three'),
      ),
    ]);
  }
}
