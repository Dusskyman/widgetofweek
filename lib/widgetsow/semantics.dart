import 'package:flutter/material.dart';

class WOWSemantics extends StatelessWidget {
  const WOWSemantics({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Semantics(
      container: true,
      child: Container(
        width: 200,
        height: 200,
        color: Colors.black,
      ),
    );
  }
}
