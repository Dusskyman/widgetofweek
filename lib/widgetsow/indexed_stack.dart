import 'package:flutter/material.dart';

class WOWIndexedStack extends StatefulWidget {
  int index = 0;
  WOWIndexedStack({Key key}) : super(key: key);

  @override
  _WOWIndexedStackState createState() => _WOWIndexedStackState();
}

class _WOWIndexedStackState extends State<WOWIndexedStack> {
  @override
  Widget build(BuildContext context) {
    return IndexedStack(
      index: widget.index,
      children: [
        ...List.generate(
          10,
          (index) => InkWell(
            onTap: () {
              setState(() {
                if (widget.index < 9) {
                  widget.index++;
                  return;
                }
                widget.index = 0;
              });
            },
            child: Container(
              alignment: Alignment.center,
              width: 400,
              height: 400,
              color: Color.fromRGBO(index * 10, index * 10, index * 10, 1),
              child: Text(
                '$index',
                style: TextStyle(color: Colors.white, fontSize: 50),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
