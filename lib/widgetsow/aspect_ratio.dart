import 'package:flutter/material.dart';

class WOWAspectRatio extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 3 / 2,
      child: Container(
        color: Colors.amber,
      ),
    );
  }
}
