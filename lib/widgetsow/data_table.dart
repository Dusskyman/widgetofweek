import 'package:flutter/material.dart';

class WOWDataTable extends StatelessWidget {
  const WOWDataTable({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DataTable(
      rows: [
        DataRow(
          cells: [
            DataCell(
              Text('Just container'),
            ),
            DataCell(
              Container(
                width: 50,
                height: 50,
                color: Colors.red,
              ),
            ),
          ],
        ),
        DataRow(
          cells: [
            DataCell(
              Text('Just another container'),
            ),
            DataCell(
              Container(
                width: 50,
                height: 50,
                color: Colors.green,
              ),
            ),
          ],
        ),
      ],
      columns: [
        DataColumn(
          label: Text('Name'),
        ),
        DataColumn(
          label: Text('Colors'),
        ),
      ],
    );
  }
}
