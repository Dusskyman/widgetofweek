import 'package:flutter/material.dart';

class WOWDismissble extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Dismissible(
        onDismissed: (direction) {},
        direction: DismissDirection.startToEnd,
        key: ValueKey('f'),
        child: Container(
          color: Colors.blue,
          width: double.infinity,
          height: 200,
        ),
      ),
    );
  }
}
