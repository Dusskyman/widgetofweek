import 'package:flutter/material.dart';

class WOWSliverAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      stretch: true,
      title: Text('Hello, sun!'),
      expandedHeight: 160,
      flexibleSpace: FlexibleSpaceBar(
        stretchModes: [
          StretchMode.zoomBackground,
        ],
        background: Expanded(
          child: Image.network(
              'https://spaceandbeyondbox.com/wp-content/uploads/2019/09/sub4_hero.jpg'),
        ),
      ),
    );
  }
}
