import 'package:flutter/material.dart';

class WOWAnimatedPositioned extends StatefulWidget {
  @override
  _WOWAnimatedPositionedState createState() => _WOWAnimatedPositionedState();
}

class _WOWAnimatedPositionedState extends State<WOWAnimatedPositioned> {
  double pos = 0;
  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      alignment: Alignment.center,
      children: [
        IconButton(
            icon: Icon(Icons.accessibility_new),
            onPressed: () {
              setState(() {
                if (pos == 0) {
                  return pos = 200;
                }
                return pos = 0;
              });
            }),
        AnimatedPositioned(
          duration: Duration(seconds: 1),
          bottom: pos,
          child: Container(
            width: 100,
            height: 100,
            color: Colors.green,
          ),
        )
      ],
    );
  }
}
