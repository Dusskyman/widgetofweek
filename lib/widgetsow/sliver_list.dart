import 'package:flutter/material.dart';
import 'package:widget_of_week/widgetsow/sliver_appbar.dart';

class WOWSliverList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 500,
      child: CustomScrollView(
        slivers: [
          WOWSliverAppBar(),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                Container(
                  width: 100,
                  height: 200,
                  color: Colors.amber,
                ),
                Padding(
                  padding: EdgeInsets.all(10),
                ),
                Container(
                  width: 100,
                  height: 200,
                  color: Colors.amber,
                ),
                Padding(
                  padding: EdgeInsets.all(10),
                ),
                Container(
                  width: 100,
                  height: 200,
                  color: Colors.amber,
                ),
                Padding(
                  padding: EdgeInsets.all(10),
                ),
                Container(
                  width: 100,
                  height: 200,
                  color: Colors.amber,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
