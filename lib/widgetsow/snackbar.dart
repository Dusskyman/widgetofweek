import 'package:flutter/material.dart';

class WOWSnackBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Builder(
        builder: (context) => Center(
          child: IconButton(
            icon: Icon(
              Icons.bubble_chart,
            ),
            onPressed: () => Scaffold.of(context).showSnackBar(
              SnackBar(
                content: Text('Wow, this is Snack Bar'),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
