import 'package:flutter/material.dart';

class WOWDivider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            width: 100,
            height: 100,
            color: Colors.blue,
          ),
          Divider(
            indent: 140,
            endIndent: 140,
            thickness: 10,
            color: Colors.grey,
          ),
          Container(
            width: 100,
            height: 100,
            color: Colors.blue,
          ),
          Divider(
            indent: 140,
            endIndent: 140,
            thickness: 10,
            color: Colors.grey,
          ),
          Container(
            width: 100,
            height: 100,
            color: Colors.blue,
          ),
          Container(
            width: 100,
            height: 100,
            color: Colors.blue,
          ),
        ],
      ),
    );
  }
}
