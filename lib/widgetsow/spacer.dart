import 'package:flutter/material.dart';

class WOWSpacer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            Container(
              color: Colors.amber,
              width: 50,
              height: 50,
            ),
            Spacer(
              flex: 3,
            ),
            Container(
              color: Colors.amber,
              width: 50,
              height: 50,
            ),
            Spacer(),
            Container(
              color: Colors.amber,
              width: 50,
              height: 50,
            ),
            Container(
              color: Colors.amber,
              width: 50,
              height: 50,
            ),
          ],
        ),
      ),
    );
  }
}
