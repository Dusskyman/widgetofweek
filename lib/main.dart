import 'dart:io';

import 'package:flutter/material.dart';
import 'package:widget_of_week/widgetsow/about_dialog.dart';
import 'package:widget_of_week/widgetsow/absorb_pointer.dart';
import 'package:widget_of_week/widgetsow/alert_dialog.dart';
import 'package:widget_of_week/widgetsow/animated_builder.dart';
import 'package:widget_of_week/widgetsow/animated_container.dart';
import 'package:widget_of_week/widgetsow/animated_cross_fade.dart';
import 'package:widget_of_week/widgetsow/animated_icon.dart';
import 'package:widget_of_week/widgetsow/animated_list.dart';
import 'package:widget_of_week/widgetsow/animated_opacity.dart';
import 'package:widget_of_week/widgetsow/animated_padding.dart';
import 'package:widget_of_week/widgetsow/animated_positioned.dart';
import 'package:widget_of_week/widgetsow/animated_switcher.dart';
import 'package:widget_of_week/widgetsow/animated_widget.dart';
import 'package:widget_of_week/widgetsow/aspect_ratio.dart';
import 'package:widget_of_week/widgetsow/backdrop_filter.dart';
import 'package:widget_of_week/widgetsow/checkbox_list_tile.dart';
import 'package:widget_of_week/widgetsow/circular_progress_indicator.dart';
import 'package:widget_of_week/widgetsow/clip_path.dart';
import 'package:widget_of_week/widgetsow/clip_rrect.dart';
import 'package:widget_of_week/widgetsow/clipoval.dart';
import 'package:widget_of_week/widgetsow/colorfilter.dart';
import 'package:widget_of_week/widgetsow/constrainedbox.dart';
import 'package:widget_of_week/widgetsow/cupertino_action_sheet.dart';
import 'package:widget_of_week/widgetsow/cupertino_activity_indicator.dart';
import 'package:widget_of_week/widgetsow/custom_paint.dart';
import 'package:widget_of_week/widgetsow/data_table.dart';
import 'package:widget_of_week/widgetsow/dismissible.dart';
import 'package:widget_of_week/widgetsow/divider.dart';
import 'package:widget_of_week/widgetsow/draggable.dart';
import 'package:widget_of_week/widgetsow/draggable_scrollable_sheet.dart';
import 'package:widget_of_week/widgetsow/drawer.dart';
import 'package:widget_of_week/widgetsow/expanded.dart';
import 'package:widget_of_week/widgetsow/fade_in_image.dart';
import 'package:widget_of_week/widgetsow/fade_transition.dart';
import 'package:widget_of_week/widgetsow/fittedbox.dart';
import 'package:widget_of_week/widgetsow/flexible.dart';
import 'package:widget_of_week/widgetsow/floatingActionButton.dart';
import 'package:widget_of_week/widgetsow/future_builder.dart';
import 'package:widget_of_week/widgetsow/hero.dart';
import 'package:widget_of_week/widgetsow/ignore_point.dart';
import 'package:widget_of_week/widgetsow/indexed_stack.dart';
import 'package:widget_of_week/widgetsow/limitedbox.dart';
import 'package:widget_of_week/widgetsow/listTile.dart';
import 'package:widget_of_week/widgetsow/listView.dart';
import 'package:widget_of_week/widgetsow/list_wheel_scroll_view.dart';
import 'package:widget_of_week/widgetsow/notification_listener.dart';
import 'package:widget_of_week/widgetsow/opacity.dart';
import 'package:widget_of_week/widgetsow/pageview.dart';
import 'package:widget_of_week/widgetsow/placeholder.dart';
import 'package:widget_of_week/widgetsow/reorderable_list_view.dart';
import 'package:widget_of_week/widgetsow/richtext.dart';
import 'package:widget_of_week/widgetsow/safearea.dart';
import 'package:widget_of_week/widgetsow/selectebleText.dart';
import 'package:widget_of_week/widgetsow/semantics.dart';
import 'package:widget_of_week/widgetsow/shader_mask.dart';
import 'package:widget_of_week/widgetsow/sizedBox.dart';
import 'package:widget_of_week/widgetsow/sliders.dart';
import 'package:widget_of_week/widgetsow/sliver_list.dart';
import 'package:widget_of_week/widgetsow/snackbar.dart';
import 'package:widget_of_week/widgetsow/spacer.dart';
import 'package:widget_of_week/widgetsow/stack.dart';
import 'package:widget_of_week/widgetsow/stream_builder.dart';
import 'package:widget_of_week/widgetsow/tabbar.dart';
import 'package:widget_of_week/widgetsow/table.dart';
import 'package:widget_of_week/widgetsow/toggle_buttons.dart';
import 'package:widget_of_week/widgetsow/tooltip.dart';
import 'package:widget_of_week/widgetsow/transform.dart';
import 'package:widget_of_week/widgetsow/tween_animation_builder.dart';
import 'package:widget_of_week/widgetsow/value_listenable_builder.dart';
import 'package:widget_of_week/widgetsow/wrap.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Widget Of Week',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: WOWReordableListView(),
      ),
    );
  }
}
